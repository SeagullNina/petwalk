import React from "react";
import styles from './Post.module.scss'
import { ReactComponent as LikeIcon } from '../assets/icons/Like.svg'
import { ReactComponent as CommentIcon } from '../assets/icons/Comment.svg'

interface IProps {
    user: string
    userPic: string
    post: string
    comments: {user: string, comment: string}[]
    date: string
}

const Post=(props:IProps)=>{
    const [like, setLike] = React.useState(false)
    const [comment, setComment] = React.useState(false)
    const [moreComments, setMoreComments] = React.useState(false)
    const {user, userPic, post, comments, date} = props;
    return(
    <div className={styles.page}>
    <div className={styles.post}>
        <div className={styles.header}>
            <div className={styles.user}>
                <img alt="" src={userPic} className={styles.userPic}/>
                {user}
            </div>
            <div className={styles.date}>
                {date}
            </div>
        </div>
        <img className={styles.picture} src={post} alt=""/>
        <div className={styles.footer}>
            <div className={styles.icons}>
                <LikeIcon className={like? styles.likeActive:styles.like} onClick={()=>{setLike(!like)}}/>
                <CommentIcon className={styles.comment} onClick={()=>{setMoreComments(false); setComment(!comment)}}/>
            </div>
            <div className={styles.comments}>
                {comments.length}
                <CommentIcon className={styles.commentReady} onClick={()=>{setComment(false); setMoreComments(!moreComments)}}/>
            </div>
        </div>
    </div>
        <div className={comment? styles.writeComment : styles.none}>
            <input placeholder="Write your comment..." className={styles.write}/>
            <img src="../Arrow.png" alt="" className={styles.send}/>
        </div>
        <div className={moreComments ? styles.moreComments : styles.none}>
            {comments.map(comment => {return <div className={styles.com}><div className={styles.comUser}> {comment.user}</div> {comment.comment}</div>})}
            <input placeholder="Write your comment..." className={styles.write}/>
            <img src="../Arrow.png" alt="" className={styles.send}/>
        </div>
    </div>)
}
export default Post