import React from "react";
import styles from './Feed.module.scss'
import Navigation from '../Navigation/Navigation'
import Post from './Post'

const posts = [
    {
        user: "Anna Lacroix",
        userPic: "../girl.png",
        post: "../post1.png",
        comments: [
            {
                user: "Bet An",
                comment: "Wow!"
            },
            {
                user: "Adele",
                comment: "Very cool!"
            },
            {
                user: "Andrey",
                comment: "How old is he?"
            },
        ],
        date: "27/11/19"
    },
    {
        user: "Boris Luger",
        userPic: "../men.png",
        post: "../post2.png",
        comments: [
            {
                user: "Evgeniy",
                comment: "Very beautiful!"
            },
            {
                user: "Nina",
                comment: "I want this dog!"
            },
        ],
        date: "26/11/19"
    }
]

const Feed = () => {
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.feed}>
            {posts.map(post => {return <Post user={post.user} userPic={post.userPic} post={post.post} comments={post.comments} date={post.date}/>})}
        </div>
    </div>
}
export default Feed