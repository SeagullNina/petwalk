import { IForm } from './IForm'

export interface IUser extends IForm {
  _id: string
  email: string
  password: string
  name: string
}
