import React from "react";
import styles from './Navigation.module.scss'
import { NavLink } from 'react-router-dom'
import { ReactComponent as HouseIcon } from '../assets/icons/House.svg'
import { ReactComponent as WorldIcon } from '../assets/icons/World.svg'
import { ReactComponent as PetIcon } from '../assets/icons/Pet.svg'
import { ReactComponent as AccountIcon } from '../assets/icons/Account.svg'

const Navigation = () =>{
    return <div className={styles.mainPage}>
        <div className={styles.navigation}>
            <div className={styles.menuBlock}>
                <NavLink
                    exact
                    to="/home/"
                    className={styles.link}
                    activeClassName={styles.selected}
                >
                    <HouseIcon className={styles.icon} />
                    <span className={styles.menuName}>Дом</span>
                </NavLink>
            </div>
            <div className={styles.menuBlock}>
                <NavLink
                    to="/pet/"
                    className={styles.link}
                    activeClassName={styles.selected}
                >
                    <PetIcon className={styles.icon} />
                    <span className={styles.menuName}>Питомцы</span>
                </NavLink>
            </div>
            <div className={styles.menuBlock}>
                <NavLink
                    to="/feed/"
                    className={styles.link}
                    activeClassName={styles.selected}
                >
                    <WorldIcon className={styles.icon} />
                    <span className={styles.menuName}>Окружение</span>
                </NavLink>
            </div>
            <div className={styles.menuBlock}>
                <NavLink
                    to="/account/"
                    className={styles.link}
                    activeClassName={styles.selected}
                >
                    <AccountIcon className={styles.icon} />
                    <span className={styles.menuName}>Аккаунт</span>
                </NavLink>
            </div>
        </div>
    </div>
}

export default Navigation