import React from 'react'
import styles from './Start.module.scss'

const Start = () => {
    return <div className={styles.page}>
        <div className={styles.firstBlock}>
            <img alt="" src="../logo.png" className={styles.logo}/>
            <img alt="" src="../Illustration.png" className={styles.illustration}/>
        </div>
        <div className={styles.secondBlock}>
            <div className={styles.description}>
                The Pet Walk app is designed for taking care of your pet.
                Fix objectives based on their needs. See their progresses and share them with the community.
            </div>
            <input type="button" className={styles.start} value="Let's start" onClick={()=>{window.location.href = '/registration/';}}/>
        </div>
    </div>
}

export default Start