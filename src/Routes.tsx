import React from 'react'
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import Start from './Start/Start'
import Registration from './Registration/Registration'
import Auth from './Auth/Auth'
import Feed from './Feed/Feed'
import Home from './Home/Home'
import Pet from './Pet/Pet'
import Account from './Account/Account'

const ProtectedRoute = ({ isLoggedIn, ...props }: any) =>
    isLoggedIn ? <Route {...props} /> : <Redirect to="/start/" />

const Routes = React.memo(() => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    {!!localStorage.getItem('logged') ? (
                        <Redirect to="/feed/" />
                    ) : (
                        <Redirect to="/start/" />
                    )}
                </Route>
                <Route path="/start/">
                    <Start/>
                </Route>
                <Route path="/registration/">
                    <Registration/>
                </Route>
                <Route path="/auth/">
                    <Auth/>
                </Route>
                <ProtectedRoute
                    isLoggedIn={!!localStorage.getItem('logged')}
                    path="/feed/"
                >
                    <Feed/>
                </ProtectedRoute>
                <ProtectedRoute
                    isLoggedIn={!!localStorage.getItem('logged')}
                    path="/home/"
                >
                    <Home/>
                </ProtectedRoute>
                <ProtectedRoute
                    isLoggedIn={!!localStorage.getItem('logged')}
                    path="/pet/"
                >
                    <Pet/>
                </ProtectedRoute>
                <ProtectedRoute
                    isLoggedIn={!!localStorage.getItem('logged')}
                    path="/account/"
                >
                    <Account/>
                </ProtectedRoute>
            </Switch>
        </BrowserRouter>
    )
})

export default Routes
