import React from "react";
import styles from './Pet.module.scss'
import Navigation from '../Navigation/Navigation'
import Card from './Card'

const dogs = [
    {
        name: "Milou",
        years: "6 months old",
        breed: "American Pit Bull Terier",
        photo: "../dog.png"
    }
]

const cats = [
    {
        name: "Lucky",
        years: "2 years old",
        breed: "Bengal Cat",
        photo: "../cat1.png"
    },
    {
        name: "Oreo",
        years: "4 years old",
        breed: "Manx Cat",
        photo: "../cat2.png"
    }
]

const Pet = () => {
    return (
        <div className={styles.page}>
            <Navigation/>
            <div className={styles.pet}>
                <div className={styles.header}>
                    My pets
                </div>
                <div className={styles.littleHeader}>
                    Dogs
                </div>
                <div className={styles.container}>
                    {dogs.map(dog => {return <Card name={dog.name} photo={dog.photo} breed={dog.breed} years={dog.years}/>})}
                </div>
                <div className={styles.littleHeader}>
                    Cats
                </div>
                <div className={styles.container}>
                    {cats.map(cat => {return <Card name={cat.name} photo={cat.photo} breed={cat.breed} years={cat.years}/>})}
                </div>
                <img alt="" src="../add.png" className={styles.add}></img>
            </div>
        </div>)
}
export default Pet