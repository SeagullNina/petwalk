import React from "react";
import styles from './Card.module.scss'

interface IProps {
    name: string,
    years: string,
    breed: string,
    photo: string
}

const Card = (props: IProps) => {
    const {name, years, breed, photo} = props
    return <div className={styles.card}>
        <img alt="" src={photo} className={styles.photo}/>
        <div className={styles.info}>
            <div className={styles.gray}>
                {breed}
            </div>
            <div className={styles.black}>
                {name}
            </div>
            <div className={styles.gray}>
                {years}
            </div>
        </div>
        <img src="../Arrow.png" alt="" className={styles.send}/>
    </div>
}

export default Card