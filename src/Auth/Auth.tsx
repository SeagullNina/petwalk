import React from 'react'
import styles from './Auth.module.scss'
import * as Validator from "../classes/validators/EmailValidator";

const Auth = () => {
    const [errorLogin, setErrorLogin] = React.useState(false);
    const [email, setEmail] = React.useState();
    const [password, setPasword] = React.useState();
    const [errorEmpty, setErrorEmpty] = React.useState(false);

    return <div className={styles.page}>
        <div className={styles.firstBlock}>
            <img alt="" src="../logo.png" className={styles.logo}/>
            <img alt="" src="../Illustration.png" className={styles.illustration}/>
        </div>
        <div className={styles.secondBlock}>
            <label className={styles.label}>Email adress</label>
            <input
                className={styles.string}
                placeholder="Email adress"
                onChange={event => {
                    setEmail(event.target.value);
                }}
                onBlur={event => {
                    if (!Validator.emailValidation(event.target.value))
                        setErrorLogin(true);
                    else setErrorLogin(false);
                }}
            />
            <label className={errorLogin ? styles.errorLabel : styles.none}>
                Неверный формат e-mail
            </label>
            <label className={styles.label}>Password</label>
            <input
                type="password"
                className={styles.string}
                placeholder="Password"
                onChange={event => {
                    setPasword(event.target.value);
                }}
            />
            <label className={errorEmpty ? styles.errorLabel : styles.none}>
                Пожалуйста, заполните все поля.
            </label>
            <input type="button" className={styles.start} value="Sign In" onClick={()=>{
                if (email && password) window.location.href = '/feed/';
            else if (!email || !password) {
                    setErrorEmpty(true);
                    return;
                }}}/>
        </div>
    </div>
}

export default Auth