import React from "react";
import styles from "./Registration.module.scss";
import UserApi from "../classes/services/api/UsersApi";
import { IUser } from "../classes/models/IUser";
import * as Validator from "../classes/validators/EmailValidator";
import Dropzone from 'react-dropzone'
import {useDropzone} from 'react-dropzone';
import Previews from './Previews'


const Registration = () => {
  const [errorLogin, setErrorLogin] = React.useState(false);
  const [name, setName] = React.useState();
  const [password, setPasword] = React.useState();
  const [email, setEmail] = React.useState();
  const [errorEmpty, setErrorEmpty] = React.useState(false);
  const handleFiles = (event: any) => console.log(event);
  return (
    <div className={styles.page}>
      <div className={styles.firstBlock}>
        <div className={styles.newUser}>New user</div>
        <div className={styles.create}>Account creation</div>
        <div className={styles.photo} onClick={handleFiles}>
            <Previews />
        </div>
      </div>
      <div className={styles.secondBlock}>
        <label className={styles.label}>Full name</label>
        <input
          className={styles.string}
          placeholder="Full name"
          onChange={event => {
            setName(event.target.value);
          }}
        />
        <label className={styles.label}>Email adress</label>
        <input
          className={styles.string}
          placeholder="Email adress"
          onChange={event => {
            setEmail(event.target.value);
          }}
          onBlur={event => {
            if (!Validator.emailValidation(event.target.value))
              setErrorLogin(true);
            else setErrorLogin(false);
          }}
        />
        <label className={errorLogin ? styles.errorLabel : styles.none}>
          Неверный формат e-mail
        </label>
        <label className={styles.label}>Password</label>
        <input
            type="password"
          className={styles.string}
          placeholder="Password"
          onChange={event => {
            setPasword(event.target.value);
          }}
        />
        <label className={errorEmpty ? styles.errorLabel : styles.none}>
          Пожалуйста, заполните все поля.
        </label>
        <input
          type="button"
          className={styles.createButton}
          value="Create my account"
          onClick={() => {
            UserApi.registration({ email, password, name } as IUser)
              .catch(res => {
                if (name && email && password) {
                  setErrorEmpty(false);
                  window.location.href = "/feed/";
                  localStorage.setItem("logged", "1");
                  localStorage.setItem("user", JSON.stringify(res));
                } else if (!name || !email || !password) {
                  setErrorEmpty(true);
                  return;
                }
              })
              .catch();
          }}
        />
          <div className={styles.login} onClick={()=>{window.location.href = "/auth/";}}>
              You have an account ? Login
          </div>
      </div>
    </div>
  );
};

export default Registration;
