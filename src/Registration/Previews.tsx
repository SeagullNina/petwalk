import React from "react";
import {useDropzone} from "react-dropzone";
import styles from "./Registration.module.scss";

const Previews = (props:any) => {
    const [file, setFile] = React.useState<any>([]);
    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: (acceptedFiles: any) => {
            setFile(Object.assign(acceptedFiles[0], {
                preview: URL.createObjectURL(acceptedFiles[0])
            }));
        }
    });

    const thumbs = (
        <div className={styles.thumb} key={file.name}>
            <div className={styles.thumbInner}>
                <img
                    src={file.preview}
                    className={styles.img}
                    alt=""
                />
            </div>
        </div>
    );
    React.useEffect(() => () => {
        URL.revokeObjectURL(file.preview);
    })
    return (
        <section className="container">
            <div {...getRootProps({className: 'dropzone'})}>
                <input {...getInputProps()} />
                <div className={styles.upload}>
                    <aside className={styles.thumbsContainer}>
                        {thumbs}
                    </aside>
                </div>
            </div>

        </section>
    );
}

export default Previews